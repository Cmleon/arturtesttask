# ArturTestTask

Simple game "Match-3" as test task with PixiJS

(1st way)
In order to run the application on your PC, download the folder "dist".
Then start any web server (I used MAMP https://www.mamp.info/en/downloads/),
put the "dist" folder in it and run server. Then just use youre browser and
navigate to your folder in the url bar (for me it's host-2:8888). 
Enjoy the game!

(2nd way)
Just visit my GitHub Pages at this link:
https://chameleon3333.github.io/ArthurTestTask/ 